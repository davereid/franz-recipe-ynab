# Franz Recipe for YNAB (You Need a Budget)

This is the unofficial [Franz 5](https://meetfranz.com/) recipe plugin for
[YNAB (You Need a Budget)](https://www.youneedabudget.com/).

## Installation

1. Clone/download the folder `franz-recipe-ynab`.

2. Open the Franz Recipe folder on your machine:
   * Mac: `~/Library/Application Support/Franz/recipes/`
   * Windows: `%appdata%/Roaming/Franz/recipes/`
   * Linux: `~/.config/Franz/recipes/`

3. Create a `dev` folder if you have not already done so.

4. Copy the `franz-recipe-ynab` folder into the recipes dev directory.

5. Restart Franz.

## Skipping the "My Budgets" Page

When you first add and login to the service you will see My Budgets listing 
page. Click the budget you want to view.

![Screenshot of My Budgets page](https://monosnap.com/image/CkCuuAy4bMnD9ALrx2GMy9XZtkPoeP.png)

To avoid this step in the future, you can update your YNAB account's settings
to open the most recent budget by default:

1. Click on your email address in the bottom left corner
   
   ![Step 1](https://monosnap.com/image/aq6Vj5pA0pZUQTjTDY6ja9NEc6YRDi.png)

2. Select My Account.
   
   ![Step 2](https://monosnap.com/image/Om4owla3cNPBJA7LGTP6XvDZWgrjzh.png)

3. Scroll down to "Options" and check the box next to "Open most recent budget
   by default."
   
   ![Step 3](https://monosnap.com/image/JHPG6PiDsUvEUTTXqxMx5aX7HsIf9p.png)

4. Click the "Back to Budget" link at the top.
   
   ![Step 4](https://monosnap.com/image/LVWynuMhH257DlXuMCgzI51eSRo0eG.png)
