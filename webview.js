"use strict";

module.exports = Franz => {
  const getMessages = function getMessages() {
    let directMessages = 0;
    let indirectMessages = document.querySelectorAll(".nav-accounts .nav-account-icons-left .svg-icon.dot").length;
    Franz.setBadge(directMessages, indirectMessages);
  };

  Franz.loop(getMessages);
};
